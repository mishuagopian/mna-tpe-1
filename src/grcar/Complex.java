package grcar;

import java.util.LinkedList;
import java.util.List;

public class Complex {

	private double real;
	private double img;
	
	public Complex(double real, double img) {
		this.real = real;
		this.img = img;
	}

	public double getReal() {
		return real;
	}

	public double getImg() {
		return img;
	}
	
	@Override
	public String toString() {
		return " " + real + (img < 0 ? "" : "+") + img + "i";
	}
	
	public static List<Complex> solveCuadraticEq(double a, double b, double c){
		List<Complex> answer = new LinkedList<Complex>();
		double determinant = b*b-4*a*c;
		if(determinant < 0){
			answer.add(new Complex(-b/(2*a), Math.sqrt(-determinant)/(2*a)));
			answer.add(new Complex(-b/(2*a), -Math.sqrt(-determinant)/(2*a)));
		} else{
			answer.add(new Complex(-b/(2*a)+Math.sqrt(determinant)/(2*a),0));
			answer.add(new Complex(-b/(2*a)-Math.sqrt(determinant)/(2*a),0));
		}
		
		return answer;
	}
	
	public static String listToOctaveVector(List<Complex> list){
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for(Complex c : list){
			sb.append(c.toString()+",");
		}
		sb.deleteCharAt(sb.length()-1);
		sb.append(' ');
		sb.append(']');
		return sb.toString();
	}
	
}
