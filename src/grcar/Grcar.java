package grcar;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Grcar {

	public static double[][] getGrcar(int n, int k){
		if(n < k+1){
			return null;
		}
		double[][] grcar = new double[n][n];
		for(int i=0; i<n; i++){
			grcar[i][i]=1;
			for(int j=0; j<k; j++){
				if(i>j){
					grcar[i-(j+1)][i]=1;
				}
			}
			if(i!=n-1){
				grcar[i+1][i]=-1;				
			}
		}
		return grcar;
	}
	
	public static double[][] gramSchmidt(double[][] m){
		int r = 0;
		double[][] gs = new double[m.length][m[0].length];
		List<double[]> columns = new LinkedList<double[]>();
		
		for (int i = 0; i < m[0].length; i++) {
			double[] column = MatrixUtils.getColumn(m, i);
			double[] aux = MatrixUtils.getColumn(m, i);
			double norm;

			// restamos la proyeccion a los ya ortonormalizados
			for (double[] c : columns) {
				aux =  MatrixUtils.vectorSubstraction(aux, MatrixUtils.vectorMultiplied(c, MatrixUtils.innerProduct(column, c)));
			}
			
			norm = MatrixUtils.norm2(aux);
			aux = MatrixUtils.vectorMultiplied(aux, 1/norm);
			columns.add(aux);
		}
		for (double[] c : columns) {
			MatrixUtils.setColumn(gs, c, r);
			r++;
		}
		return gs;
	}

	public static Map<String, double[][]> qr(double[][] m) {
		Map<String, double[][]> qr = new HashMap<String, double[][]>();
		
		double[][] gs = gramSchmidt(m);
		qr.put("Q", gs);
		
		double[][] r = MatrixUtils.matrixProduct(MatrixUtils.traspose(gs), m);
		qr.put("R", r);
		
		return qr;
	}
	
	public static double[][] qrAlgorithm(double[][] m, int N) {
		double[][] aux = m;
		Map<String, double[][]> qr;
		for (int i = 0; i < N; i++) {
			qr = qr(aux);
			aux = MatrixUtils.matrixProduct(qr.get("R"), qr.get("Q"));
		}
		return aux;
	}
	
	public static List<Complex> eig(double[][] m) {
		List<Complex> eig = new LinkedList<Complex>();
		
		double[][] qr = qrAlgorithm(m, 100+10*m.length*m.length);
		
		for (int i = 0; i < qr.length - 1; i = i + 2) {
			double[][] submatrix = MatrixUtils.submatrix(qr, i, i+1, i, i+1);
			eig.addAll(Complex.solveCuadraticEq(1, - submatrix[0][0] - submatrix[1][1], 
					submatrix[0][0] * submatrix[1][1] - submatrix[0][1] * submatrix[1][0]));
		}
		
		// veo si la dimension es impar para sacar el ultimo autovalor
		if (qr.length % 2 == 1) {
			eig.add(new Complex(qr[qr.length - 1][qr.length - 1], 0));
		}
		
		return eig;
	}
	
	public static List<Complex> eig2(double[][] m, double error, int maxAttemps){
		List<Complex> eig = new LinkedList<Complex>();
		double[][] aux = m;
		int eigsRemaining = m.length;
		double[][] submatrix;
		while(eigsRemaining > 0){
			aux = MatrixUtils.submatrix(aux, 0, eigsRemaining-1, 0, eigsRemaining-1);
			if(eigsRemaining == 1){
				eig.add(new Complex(aux[eigsRemaining-1][eigsRemaining-1], 0));
				eigsRemaining = eigsRemaining -1;
			} else if(eigsRemaining == 2){
				submatrix = MatrixUtils.submatrix(aux, eigsRemaining-2, eigsRemaining-1, eigsRemaining-2, eigsRemaining-1);
				eig.addAll(Complex.solveCuadraticEq(1, - submatrix[0][0] - submatrix[1][1], 
						submatrix[0][0] * submatrix[1][1] - submatrix[0][1] * submatrix[1][0]));
				eigsRemaining = eigsRemaining - 2;
			} else {
				int attemps = 0;
				while(Math.abs(aux[eigsRemaining-1][eigsRemaining-2]) > error && Math.abs(aux[eigsRemaining-2][eigsRemaining-3]) > error*error && attemps != maxAttemps){
					aux = Grcar.qrAlgorithm(aux, 1);
					attemps++;
				}
				if(Math.abs(aux[eigsRemaining-1][eigsRemaining-2]) < error){
					eig.add(new Complex(aux[eigsRemaining-1][eigsRemaining-1], 0));
					eigsRemaining = eigsRemaining -1;
				} else {
					submatrix = MatrixUtils.submatrix(aux, eigsRemaining-2, eigsRemaining-1, eigsRemaining-2, eigsRemaining-1);
					eig.addAll(Complex.solveCuadraticEq(1, - submatrix[0][0] - submatrix[1][1], 
							submatrix[0][0] * submatrix[1][1] - submatrix[0][1] * submatrix[1][0]));
					eigsRemaining = eigsRemaining - 2;
				}
			}
			
		}
		return eig;
	}
	
}
