package grcar;

import java.util.List;
import java.util.Map;

public class Main {
	
	private static int N = 80;
	private static int K = 3;
	
	public static void main(String[] args) {
		double[][] grcar = Grcar.getGrcar(N, K);
		long start = System.currentTimeMillis();
//		System.out.println("Grcar matrix with N: " + N + " and K: " + K + ":");
//		MatrixUtils.printMatrix(grcar);
//		System.out.println("---------------------------------------------------------------------\n");
//		
//		Map<String, double[][]> qr = Grcar.qr(grcar);
//		System.out.println("Q:");
//		MatrixUtils.printMatrix(qr.get("Q"));
//		System.out.println("\nR:");
//		MatrixUtils.printMatrix(qr.get("R"));
//		System.out.println("---------------------------------------------------------------------\n");
//		
//		System.out.println("Q*R:");
//		MatrixUtils.printMatrix(MatrixUtils.matrixProduct(qr.get("Q"), qr.get("R")));
//		System.out.println("---------------------------------------------------------------------\n");
		
		System.out.println("Eigenvalues:");
		List<Complex> eigenvalues = Grcar.eig2(grcar, 0.00001,1000);
		for (Complex c : eigenvalues) {
			System.out.println(c);
		}
		System.out.println("---------------------------------------------------------------------\n");
		System.out.println("Time elapsed:");
		System.out.println(System.currentTimeMillis() - start);
		System.out.println("\ngrcar matrix for Octave");
		System.out.println(MatrixUtils.matrixForOctave(grcar));
		System.out.println("\neigenvalues for Octave");
		System.out.println(Complex.listToOctaveVector(eigenvalues));
	}
	
}
