package grcar;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MatrixUtils {

	public static String matrixForOctave(double[][] matrix) {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				sb.append(matrix[i][j]);
				if (j != matrix[0].length - 1) {
					sb.append(',');
				}
			}
			if (i != matrix.length - 1) {
				sb.append(';');
			}
		}
		sb.append(']');
		return sb.toString();
	}

	public static String vectorForOctave(double[] vector) {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		for (int i = 0; i < vector.length; i++) {
			sb.append(vector[i]);
			if (i != vector.length - 1) {
				sb.append(',');
			}
		}
		sb.append(']');
		return sb.toString();
	}

	public static void printMatrix(double[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.print(round(matrix[i][j], 5) + "\t");
			}
			System.out.println();
		}
	}

	public static double[] getColumn(double[][] matrix, int n) {
		if (n > matrix[0].length - 1 || n < 0) {
			return null;
		}
		double[] vector = new double[matrix.length];
		for (int i = 0; i < vector.length; i++) {
			vector[i] = matrix[i][n];
		}
		return vector;
	}

	public static double[] getRow(double[][] matrix, int n) {
		if (n > matrix.length - 1 || n < 0) {
			return null;
		}
		double[] vector = new double[matrix[0].length];
		for (int i = 0; i < vector.length; i++) {
			vector[i] = matrix[n][i];
		}
		return vector;
	}

	public static double innerProduct(double[] vector1, double[] vector2) {
		double answer = 0;
		if (vector1.length != vector2.length) {
			return 0;
		}
		for (int i = 0; i < vector1.length; i++) {
			answer += vector1[i] * vector2[i];
		}
		return answer;
	}

	public static double[][] matrixProduct(double[][] matrix1,
			double[][] matrix2) {
		if (matrix1[0].length != matrix2.length) {
			return null;
		}
		double[][] answer = new double[matrix1.length][matrix2[0].length];
		for (int i = 0; i < answer.length; i++) {
			for (int j = 0; j < answer[0].length; j++) {
				answer[i][j] = MatrixUtils.innerProduct(
						MatrixUtils.getRow(matrix1, i),
						MatrixUtils.getColumn(matrix2, j));
			}
		}
		return answer;
	}

	public static double[][] matrixMultiplied(double[][] matrix, double factor) {
		double[][] answer = new double[matrix.length][matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				answer[i][j] = matrix[i][j] * factor;
			}
		}
		return answer;
	}

	public static double[] vectorMultiplied(double[] vector, double factor) {
		double[] answer = new double[vector.length];
		for (int i = 0; i < vector.length; i++) {
			answer[i] = vector[i] * factor;
		}
		return answer;
	}
	
	public static double[][] traspose(double[][] matrix){
		double[][] answer = new double[matrix[0].length][matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				answer[j][i] = matrix[i][j];
			}
		}
		return answer;
	}

	public static double norm2(double[] vector) {
		double ans = 0;
		for (int i = 0; i < vector.length; i++) {
			ans += vector[i] * vector[i];
		}
		ans = Math.sqrt(ans);
		return ans;
	}
	
	public static double[] vectorSubstraction(double[] vector1, double[] vector2){
		double[] answer = new double[vector1.length];
		for(int i=0; i<vector1.length; i++){
			answer[i]= vector1[i] - vector2[i];
		}
		return answer;
	}
	
	public static void setColumn(double[][] matrix, double[] vector, int n) {
		for (int i = 0; i < matrix.length; i++) {
			matrix[i][n] = vector[i];
		}
	}
	
	/*
	 * Extracted from http://stackoverflow.com/questions/2808535/round-a-double-to-2-decimal-places
	 */
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public static double[][] submatrix(double[][] m, int colStart, int colFinish, int rowStart, int rowFinish) {
		double[][] submatrix = new double[rowFinish - rowStart + 1][colFinish - colStart + 1];
		if (colStart > colFinish || rowStart > rowFinish || rowFinish > m.length || colFinish > m[0].length) {
			throw new IllegalArgumentException();
		}
		for (int i = 0; i <= rowFinish - rowStart; i++) {
			for (int j = 0; j <= colFinish - colStart; j++) {
				submatrix[i][j] = m[rowStart+i][colStart+j];
			}
		}
		return submatrix;
	}
	
}
