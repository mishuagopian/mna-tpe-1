package grcar;

import java.util.Vector;

public class PerformanceTest {
	public static int TEST_LIMIT = 80;
	
	public static void main(String[] args) {
		double[][] matrix;
		long start, stop;
		double[] testNumber = new double[TEST_LIMIT+1];
		double[] timeElapsed = new double[TEST_LIMIT+1];
		for(int i=4; i<=TEST_LIMIT; i++){
			System.out.println(i + " out of " + TEST_LIMIT);
			matrix = Grcar.getGrcar(i,3);
			start = System.currentTimeMillis();
			Grcar.eig2(matrix,0.00001, 1000);
			stop = System.currentTimeMillis();
			testNumber[i] = i;
			timeElapsed[i] = stop*1.0-start*1.0;
		}
		System.out.println(MatrixUtils.vectorForOctave(testNumber));
		System.out.println(MatrixUtils.vectorForOctave(timeElapsed));
	}
}
